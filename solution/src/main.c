#include "fileHandler.h"
#include "image.h"
#include "imageHandler.h"
#include "transformation.h"

void usage(void)
{
	printf("Usage: ./image-transformer BMP_FILE_NAME1 BMP_FILE_NAME2\n");
}

int main(int argc, char **argv)
{
	if (argc != 3)
	{
		usage();
		return 0;
	}

	FILE *in = NULL;
	FILE *out = NULL;
	if (!open_file(&in, argv[1], "rb"))
	{
		printf("Bad first input file\n");
		return 0;
	}
	if (!open_file(&out, argv[2], "wb"))
	{
		close_file(in);
		printf("Bad second input file\n");
		return 0;
	}

	struct image image;
	struct bitmap_header bmp;

	read_header(in, &bmp);

	from_bmp(in, &image, bmp);
	close_file(in);	

	struct image rotated_image = rotate(image);
    
	to_bmp(out, &rotated_image, bmp);

	if (image.data != NULL)
		image_destroy(&image);
	if (rotated_image.data != NULL)
		image_destroy(&rotated_image);
	close_file(out);

	return 0;
}
