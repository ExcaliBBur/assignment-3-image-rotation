#include "../include/transformation.h"

struct image rotate(struct image const source)
{
    struct image reversed_image = image_create(source.height, source.width);

    const uint64_t width = source.height;
    const uint64_t height = source.width;
    reversed_image.width = width;
    reversed_image.height = height;

    for (uint64_t i = 0; i < height; i++)
    {
        for (uint64_t j = 0; j < width; j++)
        {
            reversed_image.data[i * width + j] = source.data[(source.height - 1 - j) * source.width + i];
        }
    }

    return reversed_image;
}
