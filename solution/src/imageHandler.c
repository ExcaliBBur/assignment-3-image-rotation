#define BMP_PADDING 4
#define BIT_COUNT 24
#include "imageHandler.h"

uint8_t paddingCalculation(const struct image *img) {
    return (BMP_PADDING - img->width * sizeof(struct pixel) % BMP_PADDING) % BMP_PADDING;
}

enum read_status read_header(FILE *in, struct bitmap_header *bitmap_header) {
    if (fread(bitmap_header, sizeof(struct bitmap_header), 1, in) != sizeof(struct bitmap_header))
        return READ_INVALID_HEADER;
    if (bitmap_header->bfType[0] != 'B' || bitmap_header->bfType[1] != 'M')
        return READ_INVALID_SIGNATURE;
    if (bitmap_header->biBitCount != BIT_COUNT)
        return READ_UNKNOWN_TYPE;
    if (fseek(in, bitmap_header->bOffBits, SEEK_SET) != 0)
        return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img, struct bitmap_header bitmap_header)
{
    if (in == NULL)
        return READ_NULL_FILE;

    if (!img)
        return READ_MEMORY_ERROR;

    *img = image_create(bitmap_header.biWidth, bitmap_header.biHeight);
    if (!(img->data))
        return READ_MEMORY_ERROR;

    const uint8_t padding = paddingCalculation(img);

    for (uint32_t i = 0; i < bitmap_header.biHeight; i++)
    {
        if (fread(img->data + bitmap_header.biWidth * i, sizeof(struct pixel), bitmap_header.biWidth, in) != bitmap_header.biWidth)
        {
            return READ_INVALID_BITS;
        }

        if (fseek(in, padding, SEEK_CUR) != 0)
            return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img, struct bitmap_header bitmap_header)
{
    if (!(img))
        return WRITE_NULL_IMG;
    const uint8_t padding = paddingCalculation(img);
    const char paddingBytes[4] = {0};

    bitmap_header.biWidth = img->width;
    bitmap_header.biHeight = img->height;

    if (!fwrite(&bitmap_header, sizeof(struct bitmap_header), 1, out))
    {
        return WRITE_ERROR;
    }

    fseek(out, bitmap_header.bOffBits, SEEK_SET);
    
    for (uint32_t i = 0; i < bitmap_header.biHeight; i++)
    {
        for (uint32_t j = 0; j < bitmap_header.biWidth; j++) {
            if (fwrite(img->data + (i * bitmap_header.biWidth + j), 1, 3, out) != 3) {
                return WRITE_INVALID_BITS;
            }
        }
        if (fwrite(paddingBytes, 1, padding, out) != padding) {
            return WRITE_INVALID_BITS;
        }
    }
    return WRITE_OK;
}
