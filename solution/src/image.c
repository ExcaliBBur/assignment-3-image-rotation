#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height)
{
    return (struct image){.height = height, .width = width, .data = malloc(height * width * (sizeof(struct pixel)))};
}

void image_destroy(struct image *img)
{   
    free(img->data);
    if (img->data != NULL)
        img->data = NULL;
}
