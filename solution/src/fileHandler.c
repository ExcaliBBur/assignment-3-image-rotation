#include "fileHandler.h"

bool open_file(FILE **file, char *const filename, char *const open_mode)
{
    *file = fopen(filename, open_mode);
    return (*file != NULL);
}

bool close_file(FILE *file)
{
    return fclose(file) == 0;
}
