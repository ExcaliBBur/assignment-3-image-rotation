#pragma once

#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum read_status
{
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_NULL_FILE,
  READ_UNKNOWN_TYPE,
  READ_MEMORY_ERROR,
  /* коды других ошибок  */
};

uint8_t paddingCalculation(const struct image *img);

enum read_status read_header(FILE *in, struct bitmap_header* bitmap_header);

enum read_status from_bmp(FILE *in, struct image *img, struct bitmap_header bitmap_header);

/*  serializer   */
enum write_status
{
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_NULL_IMG,
  WRITE_INVALID_BITS,
  /* коды других ошибок  */
};

enum write_status to_bmp(FILE *out, struct image const *img, struct bitmap_header bitmap_header);
